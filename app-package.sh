set -eux;

cd /home/user/app

if [ ! -f './src/main/resources/application.properties' ]; then
      cp ./src/main/resources/application.properties.example ./src/main/resources/application.properties
      sed -i "s/INSEE_CUSTOMER_KEY/$INSEE_CUSTOMER_KEY/" ./src/main/resources/application.properties
      sed -i "s/INSEE_CUSTOMER_SECRET/$INSEE_CUSTOMER_SECRET/" ./src/main/resources/application.properties
fi

mvn clean package spring-boot:repackage