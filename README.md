### Application architecture: Hexagonale
See: https://blog.octo.com/architecture-hexagonale-trois-principes-et-un-exemple-dimplementation/
![img.png](architecture.png)

### Running application on localhost with docker compose
```shell
 docker compose up --build -d 
 ```

### Running tests on localhost with docker compose:
<b><i>Require running application on localhost with docker compose</i></b><br/><br/>
```shell
 docker compose exec spring mvn test 
 ```

### Running application image on localhost with docker
```shell
 docker build -t siret --build-arg APP_USER=$(id -u) --build-arg APP_GROUP=$(id -g) .
 ```
```shell
 mkdir -p ./siret-data/ && chown $(id -u):$(id -g) ./siret-data/
 ```
```shell
 docker run -p 8080:8080 -d -v ./siret-data:/home/user/.data-app siret
 ```

### Application documentation url: http://localhost:8080

![img.png](application.png)
