package com.zfpoly.siret.domain.company.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(JUnit4.class)
public class CompanyTest {
    @Test
    public void toList() {
        var company = new Company();
        assertArrayEquals(new String[]{"", "", "", "", ""}, company.toList().toArray());
        company
                .setSiret("siret")
                .setNic("nic")
                .setFullAdress("full-adress")
                .setFullName("full-name")
                .setCreationDate("creation-date")
        ;
        assertArrayEquals(
                new String[]{"siret", "nic", "full-name", "full-adress", "creation-date"},
                company.toList().toArray());
    }

    @Test
    public void fromList() {
        var company = new Company(List.of("siret", "nic", "full-name", "full-adress", "creation-date"));
        assertEquals("siret", company.getSiret());
        assertEquals("nic", company.getNic());
        assertEquals("full-name", company.getFullName());
        assertEquals("full-adress", company.getFullAdress());
        assertEquals("creation-date", company.getCreationDate());
        company.fromList(List.of("siret1", "nic1", "full-name1", "creation-date1", "full-adress1"));

        assertEquals("siret1", company.getSiret());
        assertEquals("nic1", company.getNic());
        assertEquals("full-name1", company.getFullName());
        assertEquals("creation-date1", company.getFullAdress());
        assertEquals("full-adress1", company.getCreationDate());

        assertThrowsExactly(
                ArrayIndexOutOfBoundsException.class,
                () -> new Company(List.of("siret", "nic", "full-name", "full-adress"))
        );
    }
}