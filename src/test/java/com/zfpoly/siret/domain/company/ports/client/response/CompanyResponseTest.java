package com.zfpoly.siret.domain.company.ports.client.response;

import com.zfpoly.siret.domain.company.model.Company;
import com.zfpoly.siret.domain.shared.ports.client.response.ResponseStatus;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class CompanyResponseTest {

    @Test
    public void success() {
        var company = new Company()
                .setNic("nic")
                .setCreationDate("date creation")
                .setFullAdress("fullAdress")
                .setSiret("siret")
                .setFullName("fullName");
        var output = new CompanyResponse().success(company);

        assertTrue(output.getSucceeded().booleanValue());
        Assert.assertEquals(ResponseStatus.OK.value(), output.getStatus());
        assertEquals(ResponseStatus.OK.getReasonPhrase(), output.getMessage());
        assertEquals(company, output.getData());
        assertNull(output.getError());

        output = new CompanyResponse().success(company, ResponseStatus.CREATED);
        assertTrue(output.getSucceeded());
        assertEquals(ResponseStatus.CREATED.value(), output.getStatus());
        assertEquals(ResponseStatus.CREATED.getReasonPhrase(), output.getMessage());
        assertEquals(company, output.getData());
        assertNull(output.getError());
    }

    @Test
    public void error() {
        var output = new CompanyResponse().error("Error message");

        assertFalse(output.getSucceeded());
        assertEquals(ResponseStatus.BAD_REQUEST.value(), output.getStatus());
        assertEquals("Error message", output.getError());
        assertNull(output.getMessage());
        assertNull(output.getData());

        output.error("Error message", ResponseStatus.FORBIDDEN);
        assertFalse(output.getSucceeded());
        assertEquals(ResponseStatus.FORBIDDEN.value(), output.getStatus());
        assertEquals("Error message", output.getError());
        assertNull(output.getMessage());
        assertNull(output.getData());

        output.error(ResponseStatus.FORBIDDEN);
        assertFalse(output.getSucceeded());
        assertEquals(ResponseStatus.FORBIDDEN.getReasonPhrase(), output.getError());
        assertNull(output.getMessage());
        assertNull(output.getData());
    }

}