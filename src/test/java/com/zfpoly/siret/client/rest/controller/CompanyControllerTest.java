package com.zfpoly.siret.client.rest.controller;

import com.zfpoly.siret.SiretApplication;
import com.zfpoly.siret.domain.company.ports.client.request.CompanyStoreRequest;
import com.zfpoly.siret.domain.company.ports.server.config.CompanyConfig;
import com.zfpoly.siret.util.JsonUtil;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.nio.file.Files;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = SiretApplication.class
)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
public class CompanyControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private CompanyConfig companyConfig;

    @After
    public void close() throws IOException {
        Files.deleteIfExists(companyConfig.csvPath());
    }

    @Test
    public void store_thenStatus200() throws Exception {
        CompanyStoreRequest companyStoreRequest = new CompanyStoreRequest();
        companyStoreRequest.setSiret("43438147100011");
        mvc.perform(post("/api/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonUtil.toJson(companyStoreRequest))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is(200)))
                .andExpect(jsonPath("$.succeeded", is(true)))
        ;

        mvc.perform(get("/api/companies/43438147100011")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is(200)))
                .andExpect(jsonPath("$.succeeded", is(true)))
                .andExpect(jsonPath("$.data.siret", is("43438147100011")))
        ;
    }

    @Test
    public void store_thenStatus404() throws Exception {
        CompanyStoreRequest companyStoreRequest = new CompanyStoreRequest();

        companyStoreRequest.setSiret("47962817400042");
        mvc.perform(post("/api/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonUtil.toJson(companyStoreRequest))
                )
                .andExpect(status().isNotFound())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is(404)))
                .andExpect(jsonPath("$.succeeded", is(false)))
                .andExpect(jsonPath("$.error", is("Company with siret [47962817400042] is not found")))
        ;
    }

    @Test
    public void store_thenStatus400() throws Exception {
        CompanyStoreRequest companyStoreRequest = new CompanyStoreRequest();

        companyStoreRequest.setSiret("siret");
        mvc.perform(post("/api/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonUtil.toJson(companyStoreRequest))
                )
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.succeeded", is(false)))
                .andExpect(jsonPath("$.error", is("Invalid siret request with value: [siret]")))
        ;
    }
}
