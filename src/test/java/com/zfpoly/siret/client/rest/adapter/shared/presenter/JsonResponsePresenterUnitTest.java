package com.zfpoly.siret.client.rest.adapter.shared.presenter;

import com.zfpoly.siret.client.rest.adapter.shared.presenter.JsonResponsePresenter;
import com.zfpoly.siret.domain.company.model.Company;
import com.zfpoly.siret.domain.company.ports.client.response.CompanyResponse;
import com.zfpoly.siret.domain.shared.ports.client.response.ResponseStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;

@RunWith(JUnit4.class)
public class JsonResponsePresenterUnitTest {

    @Test
    public void jsonPresenter_thenResponseSucceeded() {
        var jsonPresenter = new JsonResponsePresenter<CompanyResponse>();
        var company = new Company()
                .setNic("nic")
                .setCreationDate("date creation")
                .setFullAdress("fullAdress")
                .setSiret("siret")
                .setFullName("fullName");
        var output = new CompanyResponse();
        output.success(company);
        jsonPresenter.present(output);
        var response = jsonPresenter.render();

        assertInstanceOf(ResponseEntity.class, response);
        assertEquals(ResponseStatus.OK.value(), response.getStatusCode().value());
        assertEquals(output, response.getBody());

        output = new CompanyResponse();
        output.success(company, ResponseStatus.CREATED);

        jsonPresenter.present(output);
        response = jsonPresenter.render();
        assertInstanceOf(ResponseEntity.class, response);
        assertEquals(HttpStatus.CREATED.value(), response.getStatusCode().value());
        assertEquals(output, response.getBody());
    }

    @Test
    public void jsonPresenter_thenResponseNotSucceeded() {
        var jsonPresenter = new JsonResponsePresenter<CompanyResponse>();
        var output = new CompanyResponse();
        output.error("Error message");
        jsonPresenter.present(output);

        var response = jsonPresenter.render();
        assertInstanceOf(ResponseEntity.class, response);
        assertEquals(ResponseStatus.BAD_REQUEST.value(), response.getStatusCode().value());
        assertEquals(output, response.getBody());

        output.error("Error message", ResponseStatus.FORBIDDEN);
        jsonPresenter.present(output);

        response = jsonPresenter.render();
        assertInstanceOf(ResponseEntity.class, response);
        assertEquals(ResponseStatus.FORBIDDEN.value(), response.getStatusCode().value());
        assertEquals(output, response.getBody());
    }
}
