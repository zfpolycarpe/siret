package com.zfpoly.siret.server.adapter.company.service.fetcher.insee;

import com.zfpoly.siret.server.adapter.company.service.fetcher.insee.config.InseeConfig;
import com.zfpoly.siret.server.adapter.company.service.fetcher.insee.model.InseeEtablissement;
import com.zfpoly.siret.server.adapter.company.service.fetcher.insee.model.InseeUniteLegale;
import com.zfpoly.siret.server.adapter.company.service.fetcher.insee.response.InseeResponse;
import com.zfpoly.siret.server.adapter.company.service.fetcher.insee.response.InseeTokenResponse;
import com.zfpoly.siret.domain.shared.ports.client.response.exception.ResponseException;
import com.zfpoly.siret.domain.shared.ports.server.http_client.HttpClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
public class InseeResponseFetcherTest {

    @Mock(answer = Answers.CALLS_REAL_METHODS)
    HttpClient httpClient;

    @Mock(answer = Answers.CALLS_REAL_METHODS)
    InseeConfig inseeConfig;

    @Before
    public void setUp()
    {
        Mockito.when(inseeConfig.getKey()).thenReturn("key");
        Mockito.when(inseeConfig.getSecret()).thenReturn("secret");
        Mockito.when(inseeConfig.getUrlToken()).thenReturn("https://api.insee.fr/token");
        Mockito.when(inseeConfig.getUrlSiret()).thenReturn("https://api.insee.fr/entreprises/sirene/V3/siret/");
    }

    @Test
    public void fetch() throws ResponseException {
        String siret = "siret";
        String accessToken = "access_token";
        var inseeEtablissement = new InseeEtablissement()
                .setSiret(siret)
                .setNic("nic")
                .setDateCreationEtablissement("2023-02-13")
                .setUniteLegale(new InseeUniteLegale()
                        .setDenominationUniteLegale("denominationUniteLegale")
                )
        ;

        var adress = new HashMap<String, String>();
        adress.put("ville", "ville");
        adress.put("commune", null);
        adress.put("code_postale", "100");
        adress.put("departement", "  ");
        adress.put("pays", "FR");
        inseeEtablissement.setAdresseEtablissement(adress);

        var inseeCompanyOutput = new InseeResponse()
                .setEtablissement(inseeEtablissement)
        ;

        var inseeToken = new InseeTokenResponse()
                .setAccess_token(accessToken);
        Mockito.when(httpClient.post(
                InseeTokenResponse.class,
                inseeConfig.tokenUrl(),
                inseeConfig.tokenBody(),
                inseeConfig.tokenHeader()
        )).thenReturn(inseeToken);
        Mockito.when(httpClient.get(
                InseeResponse.class,
                inseeConfig.siretUrl(siret),
                inseeConfig.siretHeader(accessToken)
        )).thenReturn(inseeCompanyOutput);

        var result = new InseeCompanyFetcher(httpClient, inseeConfig).fetch(siret);
        assertTrue(result.isPresent());
        assertEquals("siret", result.get().getSiret());
        assertEquals("nic", result.get().getNic());
        assertEquals("denominationUniteLegale", result.get().getFullName());
        assertEquals("ville 100 FR", result.get().getFullAdress());
        assertEquals("2023-02-13", result.get().getCreationDate());
    }
}