package com.zfpoly.siret.server.adapter.company.config;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
public class EnvCompanyConfigTest {
    @Mock(answer = Answers.CALLS_REAL_METHODS)
    private EnvCompanyConfig companyParameterImplBagAdapter;

    private String companyCsvName;

    @Before
    public void setUp() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        companyCsvName = ".data-test/company-test-" + timestamp.getTime() + "";
    }

    @After
    public void tearDown() {
        Path path = Path.of(companyCsvName);
        try {
            Files.deleteIfExists(path);
        } catch (IOException exception) {
        }
    }

    @Test
    public void csvPath() throws IOException {
        Mockito.when(companyParameterImplBagAdapter.getCsvName()).thenReturn(companyCsvName);
        companyParameterImplBagAdapter.csvPath();

        assertTrue(Files.exists(Path.of(companyCsvName)));
        assertTrue(Files.isReadable(Path.of(companyCsvName)));
        assertTrue(Files.isWritable(Path.of(companyCsvName)));
    }
}
