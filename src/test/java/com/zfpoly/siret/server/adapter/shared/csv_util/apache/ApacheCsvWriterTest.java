package com.zfpoly.siret.server.adapter.shared.csv_util.apache;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApacheCsvWriterTest {

    private String csvName;

    @Autowired
    private ApacheCsvReader apacheCsvReader;

    @Before
    public void setUp() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        csvName = timestamp.getTime() + ".csv";
    }

    @After
    public void tearDown() throws IOException {
        Path path = Path.of(csvName);
        Files.deleteIfExists(path);
    }

    @Test
    public void insert_and_readOne() throws IOException {

        var csvPath = Path.of(csvName);
        var apacheCsvWriter = new ApacheCsvWriter(apacheCsvReader);
        apacheCsvWriter.insert(csvPath, List.of("Test00", "Test01", "Test02"));
        var data2 = List.of("Test10", "Test11", "Test12");
        apacheCsvWriter.insert(csvPath, data2);
        apacheCsvWriter.insert(csvPath, List.of("Test20", "Test21", "Test22"));
        assertTrue(Files.exists(csvPath));
        assertTrue(Files.isWritable(csvPath));
        assertTrue(Files.isReadable(csvPath));
        assertEquals(3, apacheCsvReader.read(csvPath).spliterator().getExactSizeIfKnown());
        var readOneResult = apacheCsvReader.readOne(csvPath, strings -> data2.get(0).equals(strings[0]));
        assertTrue(readOneResult.isPresent());
        assertEquals(data2.get(0), readOneResult.get()[0]);
        assertEquals(data2.get(1), readOneResult.get()[1]);
        assertEquals(data2.get(2), readOneResult.get()[2]);
        readOneResult = apacheCsvReader.readOne(csvPath, strings -> strings[2].equals("Test22"));
        assertTrue(readOneResult.isPresent());
        assertEquals("Test20", readOneResult.get()[0]);

        readOneResult = apacheCsvReader.readOne(csvPath, strings -> strings[2].equals("Test22XXX"));
        assertFalse(readOneResult.isPresent());
    }
}