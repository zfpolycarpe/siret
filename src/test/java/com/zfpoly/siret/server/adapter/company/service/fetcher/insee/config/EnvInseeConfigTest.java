package com.zfpoly.siret.server.adapter.company.service.fetcher.insee.config;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
public class EnvInseeConfigTest {

    @MockBean(answer = Answers.CALLS_REAL_METHODS)
    private EnvInseeConfig inseeApiParameterBagImpl;

    @Before
    public void setUp()
    {
        Mockito.when(inseeApiParameterBagImpl.getKey()).thenReturn("{siret.insee.customer.key}");
        Mockito.when(inseeApiParameterBagImpl.getSecret()).thenReturn("{siret.insee.customer.secret}");
        Mockito.when(inseeApiParameterBagImpl.getUrlToken()).thenReturn("https://api.insee.fr/token");
        Mockito.when(inseeApiParameterBagImpl.getUrlSiret()).thenReturn("https://api.insee.fr/entreprises/sirene/V3/siret/");
    }

    @Test
    public void tokenBody() {
        Map<String, Object> expectedResult = new HashMap<>();
        expectedResult.put("grant_type", "client_credentials");

        assertEquals(expectedResult, inseeApiParameterBagImpl.tokenBody());
    }

    @Test
    public void tokenHeader() {
        Map<String, String> expectedResult = new HashMap<>();
        expectedResult.put(HttpHeaders.AUTHORIZATION, "Basic " + Base64
                .getEncoder()
                .encodeToString((inseeApiParameterBagImpl.getKey() + ":" + inseeApiParameterBagImpl.getSecret()).getBytes()));
        expectedResult.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE);

        assertEquals(expectedResult, inseeApiParameterBagImpl.tokenHeader());
    }

    @Test
    public void siretHeader() {
        String accessToken = "access_token";
        Map<String, String> expectedResult = new HashMap<>();
        expectedResult.put(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        expectedResult.put(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);

        assertEquals(expectedResult, inseeApiParameterBagImpl.siretHeader(accessToken));
    }
}
