package com.zfpoly.siret.server.adapter.company.repository.csv;

import com.zfpoly.siret.domain.company.model.Company;
import com.zfpoly.siret.domain.company.ports.server.config.CompanyConfig;
import com.zfpoly.siret.domain.shared.ports.server.csv_util.CsvReader;
import com.zfpoly.siret.domain.shared.ports.server.csv_util.CsvWriter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class CsvCompanyRepositoryTest {
    @Autowired
    private CompanyConfig companyConfig;

    @Autowired
    @Qualifier("apacheCsvReader")
    private CsvReader reader;

    @Autowired
    @Qualifier("apacheCsvWriter")
    private CsvWriter writer;

    @Mock(answer = Answers.CALLS_REAL_METHODS)
    private CompanyConfig deniedCompanyConfig;

    private CsvCompanyRepository companyRepository;

    private CsvCompanyRepository deniedCompanyRepository;

    @Before
    public void setUp() throws IOException {
        companyRepository = new CsvCompanyRepository(companyConfig, reader, writer);
        Path accessDeniedPath = Path.of("/home/company-denied.csv");
        Mockito.when(deniedCompanyConfig.csvPath()).thenReturn(accessDeniedPath);
        deniedCompanyRepository = new CsvCompanyRepository(deniedCompanyConfig, reader, writer);
    }

    @After
    public void tearDown() {
        try {
            Files.deleteIfExists(companyConfig.csvPath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void create_update_find_delete() {
        var company = new Company().setSiret("siret");

        var resultCompany = companyRepository.create(company);
        assertTrue(resultCompany.isPresent());
        assertArrayEquals(company.toList().toArray(), resultCompany.get().toList().toArray());

        companyRepository.create(company.setSiret("siret2").setFullAdress("teste"));
        companyRepository.create(company.setSiret("siret3"));

        resultCompany = companyRepository.find("siret2");
        assertTrue(resultCompany.isPresent());
        assertEquals("siret2", resultCompany.get().getSiret());
        assertEquals("teste", resultCompany.get().getFullAdress());

        assertEquals(3, companyRepository.findAll().size());

        companyRepository.update(company.setSiret("siret2").setFullAdress("full-adress"));
        resultCompany = companyRepository.find("siret2");
        assertTrue(resultCompany.isPresent());
        assertEquals("siret2", resultCompany.get().getSiret());
        assertEquals("full-adress", resultCompany.get().getFullAdress());
        assertArrayEquals(company.toList().toArray(), resultCompany.get().toList().toArray());

        companyRepository.delete("siret3");
        resultCompany = companyRepository.find("siret3");
        assertFalse(resultCompany.isPresent());
    }

    @Test
    public void create_update_find_delete_throwsException() {
        assertThrowsExactly(CsvCompanyRepositoryException.class, () -> deniedCompanyRepository.create(new Company()));
        assertThrowsExactly(CsvCompanyRepositoryException.class, () -> deniedCompanyRepository.create(new Company()));
        assertThrowsExactly(CsvCompanyRepositoryException.class, () -> deniedCompanyRepository.find("siret"));
        assertThrowsExactly(CsvCompanyRepositoryException.class, () -> deniedCompanyRepository.findAll());
    }
}