package com.zfpoly.siret.domain.shared.ports.client.response.exception;

import com.zfpoly.siret.domain.shared.ports.client.response.ResponseStatus;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Exception thrown when status code is not valid
 */
public class InvalidResponseStatusException extends IllegalArgumentException {
    public InvalidResponseStatusException(int statusCode) {
        super("Invalid status code [" + statusCode + "]. "
                + "Allowed status code are: "
                + "["
                + Arrays
                .stream(ResponseStatus.values())
                .map(ResponseStatus::value)
                .map(Object::toString)
                .collect(Collectors.joining(", "))
                + "]"
        );
    }
}
