package com.zfpoly.siret.domain.shared.ports.server.http_client.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class HttpClient4xxErrorException extends RuntimeException {
    private final String message;
    private final Integer status;
    private final String body;

    public HttpClient4xxErrorException(String message, int statusCode, Throwable cause) {
        super(message, cause);
        this.status = getHttpStatus(statusCode).value();
        this.message = message;
        this.body = null;
    }

    public HttpClient4xxErrorException(String message, int statusCode) {
        super(message);
        this.status = getHttpStatus(statusCode).value();
        this.message = message;
        this.body = null;
    }

    public HttpClient4xxErrorException(String message, int statusCode, String body) {
        super(message);
        this.status = getHttpStatus(statusCode).value();
        this.message = message;
        this.body = body;
    }

    protected HttpStatus getHttpStatus(Integer statusCode) {
        HttpStatus httpStatus = HttpStatus.valueOf(statusCode);
        if (! isHttpStatusValide(httpStatus)) {
            throw new IllegalArgumentException("Invalid Http 4xx Client status code [" + statusCode + "]");
        }

        return httpStatus;
    }

    protected boolean isHttpStatusValide(HttpStatus httpStatus) {
        return httpStatus.is4xxClientError();
    }
}
