package com.zfpoly.siret.domain.shared.ports.client.response;

import com.zfpoly.siret.domain.shared.ports.client.response.exception.InvalidResponseStatusException;
import org.springframework.http.HttpStatus;

import java.util.Objects;
import java.util.Optional;

public enum ResponseStatus {
    OK(200),
    CREATED(201),
    ACCEPTED(202),
    BAD_REQUEST(400),
    UNAUTHORIZED(401),
    FORBIDDEN(403),
    NOT_FOUND(404);

    private final Integer value;

    ResponseStatus(Integer value) {
        this.value = value;
    }

    public Integer value() {
        return value;
    }

    public boolean isSucceeded() {
        return (value / 100) == 2;
    }

    public String getReasonPhrase() {
        return Optional
                .ofNullable(HttpStatus.resolve(value))
                .map(HttpStatus::getReasonPhrase)
                .orElse("Status " + value);
    }

    public static ResponseStatus resolve(Integer statusCode) {
        ResponseStatus[] statuses = values();
        for (ResponseStatus status : statuses) {
            if (Objects.equals(status.value, statusCode)) {
                return status;
            }
        }

        throw new InvalidResponseStatusException(statusCode);
    }
}
