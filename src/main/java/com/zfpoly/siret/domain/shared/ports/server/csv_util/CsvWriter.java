package com.zfpoly.siret.domain.shared.ports.server.csv_util;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Predicate;

public interface CsvWriter {
    /**
     * Write a row at the end of the csv file.
     * @param path the path to the file
     * @param values the row data to insert
     * @throws IOException if an I/O error occurs when writing on the file.
     */
    void insert(Path path, List<String> values) throws IOException;

    /**
     * Update all rows that match the {@link Predicate}.
     * @param path the path of file
     * @param predicate the {@link Predicate} to match with.
     * @throws IOException if an I/O error occurs when writing on the file.
     */
    void update(Path path, List<String> values, Predicate<String[]> predicate) throws IOException;

    /**
     * Delete all rows that match the {@link Predicate}
     * @param path the path of file
     * @param predicate the {@link Predicate} to match with.
     * @throws IOException if an I/O error occurs when writing on the file.
     */
    void delete(Path path, Predicate<String[]> predicate) throws IOException;
}
