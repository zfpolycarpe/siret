package com.zfpoly.siret.domain.shared.ports.client.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zfpoly.siret.domain.shared.ports.client.response.exception.ResponseException;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Optional;

@Getter
@Setter(AccessLevel.PROTECTED)
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public abstract class Response<T> {
    private Boolean succeeded;
    private Integer status;
    private String message;
    private T data;
    private String error;
    private ResponseErrorDetails errorDetails;
    
    public Response<T> success(T data, ResponseStatus responseStatus) {
        return this
                .setMessage(responseStatus.getReasonPhrase())
                .setStatus(responseStatus.value())
                .setSucceeded(true)
                .setData(data)
                .setError(null)
                .setErrorDetails(null)
                ;
    }

    public Response<T> success(T data) {
        return this.success(data, ResponseStatus.OK);
    }

    public Response<T> error(String error, ResponseStatus responseStatus, ResponseErrorDetails responseErrorDetails) {
        return this
                .setStatus(responseStatus.value())
                .setError(error)
                .setErrorDetails(responseErrorDetails)
                .setMessage(null)
                .setSucceeded(false)
                .setData(null)
                ;
    }

    public Response<T> error(String error, ResponseStatus responseStatus) {
        return error(error, responseStatus, null);
    }

    public Response<T> error(ResponseStatus responseStatus) {
        return error(responseStatus.getReasonPhrase(), responseStatus);
    }

    public Response<T> error(String error) {
        return this.error(error, ResponseStatus.BAD_REQUEST);
    }

    public Response<T> error(String error, ResponseException exception) {
        Optional.ofNullable(exception.getResponseErrorDetails())
                .ifPresentOrElse(
                        outputErrorDetails -> error(
                                error,
                                ResponseStatus.resolve(exception.getStatus()),
                                outputErrorDetails
                        ),
                        () -> error(message, ResponseStatus.resolve(exception.getStatus()))
                )
        ;

        return this;
    }

    public Response<T> error(ResponseException exception) {
        return error(exception.getError(), exception);
    }

    public Response<T> error() {
        return error(ResponseStatus.BAD_REQUEST);
    }
}
