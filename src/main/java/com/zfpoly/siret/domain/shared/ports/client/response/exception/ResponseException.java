package com.zfpoly.siret.domain.shared.ports.client.response.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zfpoly.siret.domain.shared.ports.client.response.ResponseErrorDetails;
import com.zfpoly.siret.domain.shared.ports.client.response.ResponseStatus;
import lombok.Getter;

import java.util.Objects;

@Getter
public class ResponseException extends Exception {

    private final String error;
    private final Integer status;
    private final String errorDetailsValue;
    private final Class<? extends ResponseErrorDetails> errorDetailsClass;

    public ResponseException(
            String error,
            ResponseStatus responseStatus,
            String errorDetailsValue,
            Class<? extends ResponseErrorDetails> errorDetailsClass
    ) {
        super(error);
        this.error = error;
        status = responseStatus.value();
        this.errorDetailsValue = errorDetailsValue;
        this.errorDetailsClass = errorDetailsClass;
    }

    public ResponseErrorDetails getResponseErrorDetails() {
        if (Objects.isNull(getErrorDetailsValue()) || Objects.isNull(getErrorDetailsClass())) {
            return null;
        }

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(getErrorDetailsValue(), getErrorDetailsClass());
        } catch (JsonProcessingException exception) {
            throw new IllegalArgumentException(exception.getMessage(), exception);
        }
    }
}
