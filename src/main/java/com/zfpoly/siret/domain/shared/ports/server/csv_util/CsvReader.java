package com.zfpoly.siret.domain.shared.ports.server.csv_util;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;
import java.util.function.Predicate;

public interface CsvReader {
    /**
     * Read a csv file and return all rows as a {@link Iterable} of array {@link String}.
     *
     * @param csvPath the path to the file
     * @return all rows as a list of array {@link String}
     * @throws IOException if an I/O error occurs opening the file
     */
    Iterable<String[]> read(Path csvPath) throws IOException;

    /**
     * Read a csv file and return all rows that match the predicate as {@link Iterable} of array {@link String}
     *
     * @param csvPath the path to the file
     * @param predicate {@link Predicate} with array of {@link String} argument to filter the content of file
     * @return all rows that match the predicate as a list of array {@link String}
     * @throws IOException if an I/O error occurs opening the file
     */
    Iterable<String[]> read(Path csvPath, Predicate<String[]> predicate) throws IOException;

    /**
     * Read a csv file and return the first row that match the predicate as array of {@link String}
     *
     * @param csvPath the path to the file
     * @param predicate {@link Predicate} with array of {@link String} argument to filter the content of file
     * @return all rows that match the predicate as a list of array {@link String}
     * @throws IOException if an I/O error occurs opening the file
     */
    Optional<String[]> readOne(Path csvPath, Predicate<String[]> predicate) throws IOException;
}
