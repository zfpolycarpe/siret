package com.zfpoly.siret.domain.shared.ports.server.http_client;

import com.zfpoly.siret.domain.shared.ports.server.http_client.exception.HttpClient4xxErrorException;

import java.util.Map;

public interface HttpClient {
    /**
     *
     * @param elementClass the response type to decode to
     * @param url the url to call
     * @param body the request body
     * @param headers the resquest header
     * @return the decoded body
     * @param <T> the target body type
     * @throws HttpClient4xxErrorException if error occurs when request is performed
     */
    <T> T post(
            Class<T> elementClass,
            String url,
            Map<String, Object> body,
            Map<String, String> headers
    ) throws HttpClient4xxErrorException;

    /**
     * Perform a HTTP GET request
     *
     * @param elementClass the response type to decode to
     * @param url the url to call
     * @param headers the resquest header
     * @return the decoded body
     * @param <T> the target body type
     * @throws HttpClient4xxErrorException if error occurs when request is performed
     */
    <T> T get(
            Class<T> elementClass,
            String url,
            Map<String, String> headers
    ) throws HttpClient4xxErrorException;
}
