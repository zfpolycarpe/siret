package com.zfpoly.siret.domain.shared.ports.client.response;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Response404 extends Response<Object> {
    @Override
    @JsonIgnore
    public Object getData() {
        return super.getData();
    }

    @Override
    @JsonIgnore
    public String getMessage() {
        return super.getMessage();
    }

    @Override
    @JsonIgnore
    public ResponseErrorDetails getErrorDetails() {
        return super.getErrorDetails();
    }
}
