package com.zfpoly.siret.domain.shared.ports.client.presenter;

import com.zfpoly.siret.domain.shared.ports.client.response.Response;

public interface ResponsePresenter<O extends Response<?>, R> {
    /**
     * Presents a {@link Response} for a futur rendering.
     * @param output the output to render
     */
    void present(O output);

    /**
     * Render the {@link Response} presented from {@link ResponsePresenter#present(Response)}
     * @return a rendered response
     */
    R render();
}
