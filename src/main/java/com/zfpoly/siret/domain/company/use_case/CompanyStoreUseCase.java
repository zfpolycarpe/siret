package com.zfpoly.siret.domain.company.use_case;

import com.zfpoly.siret.domain.company.ports.client.request.CompanyStoreRequest;
import com.zfpoly.siret.domain.company.ports.client.response.CompanyResponse;
import com.zfpoly.siret.domain.shared.ports.client.presenter.ResponsePresenter;

public interface CompanyStoreUseCase {
    /**
     * Fetch a company from external source and update the local system database:<br/>
     * <p>
     *     - Insert a new company if the fetched company exists and not found on local system database.<br/>
     *     - Update the local system database company if the fetched company is found inside it.<br/>
     *     - Remove company form local system database if company not found on fetch.<br/>
     * </p>
     * @param request information about the company to store
     * @param responsePresenter the resposnse responsePresenter
     */
    void execute(CompanyStoreRequest request, ResponsePresenter<CompanyResponse, ?> responsePresenter);
}
