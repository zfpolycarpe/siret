package com.zfpoly.siret.domain.company.ports.client.response;

import com.zfpoly.siret.domain.company.model.Company;
import com.zfpoly.siret.domain.shared.ports.client.response.Response;

public class CompanyResponse extends Response<Company> {
}
