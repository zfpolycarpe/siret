package com.zfpoly.siret.domain.company.ports.client.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@ToString
public class CompanyStoreRequest {
    @NotBlank
    private String siret;
}
