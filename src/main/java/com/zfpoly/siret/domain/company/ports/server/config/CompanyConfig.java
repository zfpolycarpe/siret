package com.zfpoly.siret.domain.company.ports.server.config;

import java.io.IOException;
import java.nio.file.Path;

public interface CompanyConfig {

    /**
     * Get the csv file {@link Path} that contains companies.
     * If the file doesn't exist, attempts to create a new one.
     *
     * @return a {@link Path} of the company's csv file
     * @throws IOException if an I/O error occurs
     */
    Path csvPath() throws IOException;
}
