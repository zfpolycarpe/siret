package com.zfpoly.siret.domain.company.ports.client.response;

import com.zfpoly.siret.domain.company.model.Company;
import com.zfpoly.siret.domain.shared.ports.client.response.Response;

import java.util.List;

public class CompanyListResponse extends Response<List<Company>> {
}
