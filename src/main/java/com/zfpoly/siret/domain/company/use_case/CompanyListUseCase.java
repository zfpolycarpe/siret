package com.zfpoly.siret.domain.company.use_case;

import com.zfpoly.siret.domain.company.ports.client.response.CompanyListResponse;
import com.zfpoly.siret.domain.shared.ports.client.presenter.ResponsePresenter;

public interface CompanyListUseCase {
    /**
     * Presents list of all companies.
     *
     * @param responsePresenter a response presenter
     */
    void execute(ResponsePresenter<CompanyListResponse, ?> responsePresenter);
}
