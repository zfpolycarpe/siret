package com.zfpoly.siret.domain.company.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Optional;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@ToString
public class Company {

    private String siret;

    private String nic;

    private String fullName;

    private String fullAdress;

    private String creationDate;

    public Company(List<String> strings) {
        this.fromList(strings);
    }

    public void fromList(List<String> strings) {
        this.setSiret(strings.get(0))
                .setNic(strings.get(1))
                .setFullName(strings.get(2))
                .setFullAdress(strings.get(3))
                .setCreationDate(strings.get(4))
        ;
    }

    /**
     * Convert a company to list of string.
     */
    public List<String> toList() {
        return List.of(
                Optional.ofNullable(siret).orElse(""),
                Optional.ofNullable(nic).orElse(""),
                Optional.ofNullable(fullName).orElse(""),
                Optional.ofNullable(fullAdress).orElse(""),
                Optional.ofNullable(creationDate).orElse("")
        );
    }
}
