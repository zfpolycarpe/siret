package com.zfpoly.siret.domain.company.use_case;

import com.zfpoly.siret.domain.company.ports.client.response.CompanyResponse;
import com.zfpoly.siret.domain.shared.ports.client.presenter.ResponsePresenter;

public interface CompanyReadUseCase {
    /**
     * Presents one company who matches the siret.
     *
     * @param siret the company siret
     * @param responsePresenter a response presenter.
     */
    void execute(String siret, ResponsePresenter<CompanyResponse, ?> responsePresenter);
}
