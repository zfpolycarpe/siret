package com.zfpoly.siret.domain.company.use_case;

import com.zfpoly.siret.domain.company.ports.client.response.CompanyResponse;
import com.zfpoly.siret.domain.company.ports.server.repository.CompanyRepository;
import com.zfpoly.siret.domain.shared.ports.client.presenter.ResponsePresenter;
import com.zfpoly.siret.domain.shared.ports.client.response.ResponseStatus;
import com.zfpoly.siret.domain.shared.use_case.UseCase;
import org.springframework.beans.factory.annotation.Autowired;

@UseCase
public class CompanyReadUseCaseImpl implements CompanyReadUseCase {

    private final CompanyRepository companyRepository;

    @Autowired
    public CompanyReadUseCaseImpl(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @Override
    public void execute(String siret, ResponsePresenter<CompanyResponse, ?> responsePresenter) {
        var response = new CompanyResponse();
        var optionalCompany = companyRepository.find(siret);
        optionalCompany
                .ifPresentOrElse(
                        response::success,
                        () -> response.error(
                                "Company with siret [" + siret + "] not found.",
                                ResponseStatus.NOT_FOUND
                        )
                );

        responsePresenter.present(response);
    }
}
