package com.zfpoly.siret.domain.company.use_case;

import com.zfpoly.siret.domain.company.model.Company;
import com.zfpoly.siret.domain.company.ports.client.request.CompanyStoreRequest;
import com.zfpoly.siret.domain.company.ports.client.response.CompanyResponse;
import com.zfpoly.siret.domain.company.ports.server.repository.CompanyRepository;
import com.zfpoly.siret.domain.company.ports.server.service.fetcher.CompanyFetcherService;
import com.zfpoly.siret.domain.shared.ports.client.presenter.ResponsePresenter;
import com.zfpoly.siret.domain.shared.ports.client.response.ResponseStatus;
import com.zfpoly.siret.domain.shared.ports.client.response.exception.ResponseException;
import com.zfpoly.siret.domain.shared.use_case.UseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@UseCase
public class CompanyStoreUseCaseImpl implements CompanyStoreUseCase {

    private final CompanyFetcherService companyFetcher;
    private final CompanyRepository companyRepository;

    @Autowired
    public CompanyStoreUseCaseImpl(
            @Qualifier("inseeCompanyFetcher") CompanyFetcherService companyFetcher,
            @Qualifier("csvCompanyRepository") CompanyRepository companyRepository
    ) {
        this.companyFetcher = companyFetcher;
        this.companyRepository = companyRepository;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void execute(CompanyStoreRequest request, ResponsePresenter<CompanyResponse, ?> responsePresenter) {
        var response = new CompanyResponse();
        String siret = request.getSiret();
        try {
            var optionalFetchedCompany = companyFetcher.fetch(siret);
            if (optionalFetchedCompany.isEmpty()) {
                companyRepository.delete(siret);
                response.error(
                        "Company with siret [" + siret + "] is not found",
                        ResponseStatus.NOT_FOUND
                );
            } else {
                var fetchedCompany = optionalFetchedCompany.get();
                companyRepository
                        .find(siret)
                        .ifPresentOrElse(
                                company -> updateCompany(fetchedCompany, response),
                                () -> createCompany(fetchedCompany, response)
                        )
                ;
            }
        } catch (ResponseException responseException) {
            response.error(responseException);
        }

        responsePresenter.present(response);
    }

    /**
     * Insert inside local system database a new company from the fetched company.
     *
     * @param fetchedCompany the company fetched
     * @param response {@link CompanyResponse}
     */
    private void createCompany(Company fetchedCompany, CompanyResponse response) {
        companyRepository.create(fetchedCompany)
                .ifPresentOrElse(
                        response::success,
                        () -> response.error(
                                "An error occured when creating company with siret"
                                        + " ["
                                        + fetchedCompany.getSiret()
                                        + "]"
                        )
                )
        ;
    }

    /**
     * Update from the fetched company the company inside local system database.
     *
     * @param fetchedCompany the company fetched
     * @param response {@link CompanyResponse}
     */
    private void updateCompany(Company fetchedCompany, CompanyResponse response) {
        var updatedCompany = companyRepository.update(fetchedCompany);
        updatedCompany
                .ifPresentOrElse(
                        response::success,
                        () -> response.error(
                                "An error occured when updating company with siret ["
                                        + fetchedCompany.getSiret()
                                        + "]"
                        )
                )
        ;
    }
}
