package com.zfpoly.siret.domain.company.ports.server.repository;

import com.zfpoly.siret.domain.company.model.Company;

import java.util.List;
import java.util.Optional;

public interface CompanyRepository {
    /**
     * Save a company to local system storage.
     * @param company to save
     */
    Optional<Company> create(Company company);

    /**
     * Update a company in local system storage.
     * @param company company to update
     * @return {@link Optional<Company>} the updated company
     */
    Optional<Company> update(Company company);

    /**
     * Delete a company from local system storage.
     *
     * @param siret the siret of company to delete
     * @return true if deleted, false otherwise
     */
    boolean delete(String siret);

    /**
     * Find one company stored in  local system storage by siret
     * @param siret the siret of company to find.
     * @return {@link Optional<Company>} present if company with siret is founded.
     */
    Optional<Company> find(String siret);

    /**
     * Find all company stored in local system storage
     * @return list of all companies.
     */
    List<Company> findAll();
}
