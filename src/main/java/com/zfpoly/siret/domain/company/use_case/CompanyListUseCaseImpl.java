package com.zfpoly.siret.domain.company.use_case;

import com.zfpoly.siret.domain.company.ports.client.response.CompanyListResponse;
import com.zfpoly.siret.domain.company.ports.server.repository.CompanyRepository;
import com.zfpoly.siret.domain.shared.ports.client.presenter.ResponsePresenter;
import com.zfpoly.siret.domain.shared.use_case.UseCase;
import org.springframework.beans.factory.annotation.Autowired;

@UseCase
public class CompanyListUseCaseImpl implements CompanyListUseCase {

    private final CompanyRepository companyRepository;

    @Autowired
    public CompanyListUseCaseImpl(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @Override
    public void execute(ResponsePresenter<CompanyListResponse, ?> presenter) {
        var response = new CompanyListResponse();
        var companies = companyRepository.findAll();
        response.success(companies);

        presenter.present(response);
    }
}
