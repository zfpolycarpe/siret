package com.zfpoly.siret.domain.company.ports.server.service.fetcher;

import com.zfpoly.siret.domain.company.model.Company;
import com.zfpoly.siret.domain.shared.ports.client.response.exception.ResponseException;

import java.util.Optional;

public interface CompanyFetcherService {
    /**
     * Fetch a company from an external source of system.
     *
     * @param siret the siret of company to fetch
     * @return an optional company
     * @throws ResponseException if an external client error occures when fetching company.
     */
    Optional<Company> fetch(String siret) throws ResponseException;
}
