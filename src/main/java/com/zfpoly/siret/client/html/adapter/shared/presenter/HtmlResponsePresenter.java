package com.zfpoly.siret.client.html.adapter.shared.presenter;

import com.zfpoly.siret.domain.shared.ports.client.presenter.ResponsePresenter;
import com.zfpoly.siret.domain.shared.ports.client.response.Response;
import org.springframework.web.servlet.ModelAndView;

public class HtmlResponsePresenter<O extends Response<?>> implements ResponsePresenter<O, ModelAndView> {
    private final String viewName;
    private O output;

    public HtmlResponsePresenter(String viewName) {
        this.viewName = viewName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void present(O output) {
        this.output = output;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ModelAndView render() {
        var view = new ModelAndView(this.viewName);
        view.addObject("output", output);

        return view;
    }
}
