package com.zfpoly.siret.client.rest.controller;

import com.zfpoly.siret.client.rest.adapter.shared.presenter.JsonResponsePresenter;
import com.zfpoly.siret.domain.company.ports.client.request.CompanyStoreRequest;
import com.zfpoly.siret.domain.company.ports.client.response.CompanyListResponse;
import com.zfpoly.siret.domain.company.ports.client.response.CompanyResponse;
import com.zfpoly.siret.domain.company.use_case.CompanyListUseCase;
import com.zfpoly.siret.domain.company.use_case.CompanyReadUseCase;
import com.zfpoly.siret.domain.company.use_case.CompanyStoreUseCase;
import com.zfpoly.siret.domain.shared.ports.client.presenter.ResponsePresenter;
import com.zfpoly.siret.domain.shared.ports.client.response.Response400;
import com.zfpoly.siret.domain.shared.ports.client.response.Response404;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/companies", produces= MediaType.APPLICATION_JSON_VALUE)
public class CompanyController {

    private final CompanyStoreUseCase companyStore;
    private final CompanyReadUseCase companyRead;
    private final CompanyListUseCase companyList;

    @Autowired
    public CompanyController(
            CompanyStoreUseCase companyStore,
            CompanyReadUseCase companyRead,
            CompanyListUseCase companyList
    ) {
        this.companyStore = companyStore;
        this.companyRead = companyRead;
        this.companyList = companyList;
    }

    /**
     * @see CompanyStoreUseCase#execute(CompanyStoreRequest, ResponsePresenter)
     * @return Array of companies in json format
     */
    @Operation(summary = "Fetch a company from external source and update local database")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Found the company",
                    content = @Content(
                           schema = @Schema(implementation = CompanyResponse.class)
                    )
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Invalid input is submitted.",
                    content = @Content(
                            schema = @Schema(implementation = Response400.class)
                    )
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Company not found",
                    content = @Content(
                            schema = @Schema(implementation = Response404.class)
                    )
            )
    })
    @PostMapping(path = {""})
    public ResponseEntity<CompanyResponse> store(@Valid @RequestBody CompanyStoreRequest input) {
        var presenter = new JsonResponsePresenter<CompanyResponse>();
        companyStore.execute(input, presenter);

        return presenter.render();
    }

    /**
     * @see CompanyListUseCase#execute(ResponsePresenter)
     * @return Array of companies in json format
     */
    @Operation(summary = "Get all companies present on local system database")
    @ApiResponse(
            responseCode = "200",
            content = @Content(
                    schema = @Schema(implementation = CompanyListResponse.class)
            )
    )
    @GetMapping(path = {""})
    public ResponseEntity<CompanyListResponse> list() {
        var presenter = new JsonResponsePresenter<CompanyListResponse>();
        companyList.execute(presenter);

        return presenter.render();
    }

    /**
     * @see CompanyReadUseCase#execute(String, ResponsePresenter)
     * @return a company having the siret
     */
    @Operation(summary = "Get the company presents in local system database having the siret given.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Found the company",
                    content = @Content(
                            schema = @Schema(implementation = CompanyResponse.class)
                    )
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Company not found",
                    content = @Content(
                            schema = @Schema(implementation = Response404.class)
                    )
            )
    })
    @GetMapping(path = {"/{siret}"})
    public ResponseEntity<CompanyResponse> read(@PathVariable String siret) {
        var presenter = new JsonResponsePresenter<CompanyResponse>();
        companyRead.execute(siret, presenter);

        return presenter.render();
    }
}
