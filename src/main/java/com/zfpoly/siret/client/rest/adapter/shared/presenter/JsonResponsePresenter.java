package com.zfpoly.siret.client.rest.adapter.shared.presenter;

import com.zfpoly.siret.domain.shared.ports.client.presenter.ResponsePresenter;
import com.zfpoly.siret.domain.shared.ports.client.response.Response;
import org.springframework.http.ResponseEntity;

public class JsonResponsePresenter<O extends Response<?>> implements ResponsePresenter<O , ResponseEntity<O>> {

    private O output;

    /**
     * {@inheritDoc}
     */
    @Override
    public void present(O output) {
        this.output = output;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<O> render() {
        return ResponseEntity.status(output.getStatus()).body(output);
    }
}
