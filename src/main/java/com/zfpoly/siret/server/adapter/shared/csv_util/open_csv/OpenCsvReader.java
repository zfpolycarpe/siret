package com.zfpoly.siret.server.adapter.shared.csv_util.open_csv;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import com.zfpoly.siret.domain.shared.ports.server.csv_util.CsvReader;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.function.Predicate;

@Service
public class OpenCsvReader implements CsvReader {
    /**
     * {@inheritDoc}
     */
    @Override
    public Iterable<String[]> read(Path csvPath) throws IOException {
        try (var csvReader = getCsvReader(csvPath)) {
            return csvReader.readAll();
        } catch (CsvException csvException) {
            throw new IOException(csvException.getMessage(), csvException);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterable<String[]> read(Path csvPath, Predicate<String[]> predicate) throws IOException {
        try (var csvReader = getCsvReader(csvPath)) {
            return csvReader.readAll().stream().filter(predicate).toList();
        } catch (CsvException csvException) {
            throw new IOException(csvException.getMessage(), csvException);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<String[]> readOne(Path csvPath, Predicate<String[]> predicate) throws IOException {
        try (var csvReader = getCsvReader(csvPath)) {
            String[] nextRecord;
            while ((nextRecord = csvReader.readNext()) != null) {
                if (predicate.test(nextRecord)) {
                    return Optional.of(nextRecord);
                }
            }
        } catch (CsvException csvException) {
            throw new IOException(csvException.getMessage(), csvException);
        }

        return Optional.empty();
    }

    /**
     * @param csvPath the path to the file
     * @return a {@link BufferedReader} of the path to the file
     * @throws IOException if an I/O error occurs reading the file
     */
    private BufferedReader getBufferedReader(Path csvPath) throws IOException {
        return Files.newBufferedReader(csvPath);
    }

    /**
     * @param csvPath the path to the file
     * @return a {@link BufferedReader} of the path to the file
     * @throws IOException if an I/O error occurs reading the file
     */
    private CSVReader getCsvReader(Path csvPath) throws IOException {
        var reader = getBufferedReader(csvPath);
        return new CSVReader(reader);
    }
}
