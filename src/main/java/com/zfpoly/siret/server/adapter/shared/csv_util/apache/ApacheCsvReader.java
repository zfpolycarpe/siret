package com.zfpoly.siret.server.adapter.shared.csv_util.apache;

import com.zfpoly.siret.domain.shared.ports.server.csv_util.CsvReader;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

@Service
public class ApacheCsvReader implements CsvReader {

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterable<String[]> read(Path csvPath) throws IOException {
        try (BufferedReader reader = getBufferedReader(csvPath)) {
            return getStream(reader).toList();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterable<String[]> read(Path csvPath, Predicate<String[]> predicate) throws IOException {
        try (BufferedReader reader = getBufferedReader(csvPath)) {
            return getStream(reader).filter(predicate).toList();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<String[]> readOne(Path csvPath, Predicate<String[]> predicate) throws IOException {
        try (BufferedReader reader = getBufferedReader(csvPath)) {
            return getStream(reader).filter(predicate).findFirst();
        }
    }

    /**
     * @param csvPath the path to the file
     * @return a {@link BufferedReader} of the path to the file
     * @throws IOException if an I/O error occurs reading the file
     */
    private BufferedReader getBufferedReader(Path csvPath) throws IOException {
        return Files.newBufferedReader(csvPath);
    }

    /**
     * @param reader a {@link BufferedReader} of the path to the file
     * @return a stream containing of  array of string
     * @throws IOException if an I/O error occures
     */
    private Stream<String[]> getStream(BufferedReader reader) throws IOException {
        return CSVFormat.DEFAULT.parse(reader).getRecords().stream().map(CSVRecord::values);
    }
}
