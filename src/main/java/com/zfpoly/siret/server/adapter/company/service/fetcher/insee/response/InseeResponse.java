package com.zfpoly.siret.server.adapter.company.service.fetcher.insee.response;

import com.zfpoly.siret.domain.company.model.Company;
import com.zfpoly.siret.server.adapter.company.service.fetcher.insee.model.InseeEtablissement;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@Accessors(chain = true)
public class InseeResponse {
    private InseeEtablissement etablissement;

    public Optional<Company> toCompany() {
        if (Objects.isNull(etablissement)) {
            return Optional.empty();
        }

        return Optional.of(new Company()
                .setSiret(etablissement.getSiret())
                .setNic(etablissement.getNic())
                .setCreationDate(etablissement.getDateCreationEtablissement())
                .setFullName(etablissement.getUniteLegale().getDenominationUniteLegale())
                .setFullAdress(etablissement.getAdresseEtablissement()
                        .values()
                        .stream()
                        .filter(adress -> !Objects.isNull(adress) && !adress.trim().isEmpty())
                        .map(Object::toString)
                        .collect(Collectors.joining(" "))
                )
        );
    }
}
