package com.zfpoly.siret.server.adapter.company.service.fetcher.insee.config;

import java.util.Map;

public interface InseeConfig {
    String getKey();

    String getSecret();

    String getUrlToken();

    String getUrlSiret();

    String tokenUrl();

    String siretUrl(String siret);

    Map<String, Object> tokenBody();

    Map<String, String> tokenHeader();

    Map<String, String> siretHeader(String accessToken);
}
