package com.zfpoly.siret.server.adapter.company.service.fetcher.insee.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class InseeHeader {
    private int statut;
    private String message;
}
