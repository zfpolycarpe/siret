package com.zfpoly.siret.server.adapter.shared.csv_util.apache;

import com.zfpoly.siret.domain.shared.ports.server.csv_util.CsvReader;
import com.zfpoly.siret.domain.shared.ports.server.csv_util.CsvWriter;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

@Service
public class ApacheCsvWriter implements CsvWriter {

    private final CsvReader csvReader;

    @Autowired
    public ApacheCsvWriter(@Qualifier("apacheCsvReader") CsvReader csvReader) {
        this.csvReader = csvReader;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void insert(Path path, List<String> values) throws IOException {
        try (
                BufferedWriter writer = Files.newBufferedWriter(
                        path,
                        StandardOpenOption.APPEND,
                        StandardOpenOption.CREATE);
                CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT)
        ) {
            printer.printRecord(values);
            printer.flush();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(Path path, List<String> values, Predicate<String[]> predicate) throws IOException {
        var oldRecords = csvReader.read(path);
        var newRecords = new ArrayList<String[]>();
        for (String[] element : oldRecords) {
            if (predicate.test(element)) {
                newRecords.add(values.toArray(new String[0]));
                continue;
            }
            newRecords.add(element);
        }

        try (
                BufferedWriter writer = Files.newBufferedWriter(
                        path,
                        StandardOpenOption.TRUNCATE_EXISTING,
                        StandardOpenOption.CREATE);
                CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT)
        ) {
            printer.printRecords(newRecords);
            printer.flush();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(Path path, Predicate<String[]> predicate) throws IOException {
        var oldRecords = csvReader.read(path);
        var newRecords = new ArrayList<String[]>();
        var recordIterator = oldRecords.iterator();
        var oldValueExists = false;
        while (recordIterator.hasNext()) {
            var element = recordIterator.next();
            if (predicate.test(element)) {
                oldValueExists = true;
                continue;
            }
            newRecords.add(element);
        }
        if (! oldValueExists) {
            return;
        }
        try (
                BufferedWriter writer = Files.newBufferedWriter(
                        path,
                        StandardOpenOption.TRUNCATE_EXISTING,
                        StandardOpenOption.CREATE
                );
                CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT)
        ) {
            printer.printRecords(newRecords);
            printer.flush();
        }
    }
}
