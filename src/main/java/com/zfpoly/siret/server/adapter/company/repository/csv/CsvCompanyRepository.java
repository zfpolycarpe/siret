package com.zfpoly.siret.server.adapter.company.repository.csv;

import com.zfpoly.siret.domain.company.model.Company;
import com.zfpoly.siret.domain.company.ports.server.config.CompanyConfig;
import com.zfpoly.siret.domain.company.ports.server.repository.CompanyRepository;
import com.zfpoly.siret.domain.shared.ports.server.csv_util.CsvReader;
import com.zfpoly.siret.domain.shared.ports.server.csv_util.CsvWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class CsvCompanyRepository implements CompanyRepository {
    private final CompanyConfig companyConfig;
    private final CsvReader csvReader;
    private final CsvWriter csvWriter;

    @Autowired
    public CsvCompanyRepository(
            CompanyConfig companyConfig,
            @Qualifier("apacheCsvReader") CsvReader csvReader,
            @Qualifier("apacheCsvWriter") CsvWriter csvWriter
    ) {
        this.companyConfig = companyConfig;
        this.csvReader = csvReader;
        this.csvWriter = csvWriter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Company> create(Company company) {
        try {
            csvWriter.insert(companyConfig.csvPath(), company.toList());
        } catch (IOException ioException) {
            throw new CsvCompanyRepositoryException(ioException);
        }

        return Optional.of(company);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Company> update(Company company) {
        try {
            csvWriter.update(
                    companyConfig.csvPath(),
                    company.toList(),
                    strings ->strings[0].equals(company.getSiret())
            );
        } catch (IOException ioException) {
            throw new CsvCompanyRepositoryException(ioException);
        }

        return Optional.of(company);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean delete(String siret) {
        try {
            csvWriter.delete(companyConfig.csvPath(), strings ->strings[0].equals(siret));
        } catch (IOException ioException) {
            throw new CsvCompanyRepositoryException(ioException);
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Company> find(String siret) {
        Company company = null;
        try {
            Optional<String[]> optionalRecord = csvReader.readOne(
                    companyConfig.csvPath(),
                    strings -> strings[0].equals(siret)
            );
            if (optionalRecord.isPresent()) {
                company = new Company();
                company.fromList(List.of(optionalRecord.get()));
            }
        } catch (IOException ioException) {
            throw new CsvCompanyRepositoryException(ioException);
        }

        return Optional.ofNullable(company);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Company> findAll() {
        var companies = new ArrayList<Company>();
        try {
            csvReader.read(companyConfig.csvPath())
                    .forEach(csvRecord -> companies.add(new Company(List.of(csvRecord))))
            ;
        } catch (IOException ioException) {
            throw new CsvCompanyRepositoryException(ioException);
        }
        return companies;
    }
}
