package com.zfpoly.siret.server.adapter.company.service.fetcher.insee.response;

import com.zfpoly.siret.domain.shared.ports.client.response.ResponseErrorDetails;
import com.zfpoly.siret.server.adapter.company.service.fetcher.insee.model.InseeHeader;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Insee4xxError implements ResponseErrorDetails {
    private InseeHeader header;
}
