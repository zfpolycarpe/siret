package com.zfpoly.siret.server.adapter.shared.http_client.rest_template;

import com.zfpoly.siret.domain.shared.ports.server.http_client.HttpClient;
import com.zfpoly.siret.domain.shared.ports.server.http_client.exception.HttpClient4xxErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Map;

@Service
public class RestTemplateHttpClient implements HttpClient {

    private final RestTemplate restTemplate;

    private static final TrustManager[] TRUST_MANAGERS = new TrustManager[] {
            new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }

                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    //Ignore check client is trusted
                }

                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    //Ignore check client is trusted
                }
            }
    };

    @Autowired
    public RestTemplateHttpClient() throws NoSuchAlgorithmException, KeyManagementException {
        this.restTemplate = getWebClient();
    }

    @Override
    public <T> T post(
            Class<T> elementClass,
            String url,
            Map<String, Object> body,
            Map<String, String> headers
    ) throws HttpClient4xxErrorException {
        HttpHeaders httpHeaders = new HttpHeaders();
        headers.forEach(httpHeaders::add);

        var strBody = new StringBuilder();
        body.forEach((key, val) -> strBody.append(key).append("=").append(val.toString()).append("&"));
        var request = new HttpEntity<>(strBody.toString(), httpHeaders);

        try {
            return restTemplate
                    .exchange(url, HttpMethod.POST, request, elementClass)
                    .getBody()
            ;
        } catch (HttpClientErrorException clientErrorException) {
            if (! clientErrorException.getStatusCode().is4xxClientError()) {
                throw clientErrorException;
            }
            throw new HttpClient4xxErrorException(
                    clientErrorException.getMessage(),
                    clientErrorException.getStatusCode().value(),
                    clientErrorException
            );
        }
    }

    @Override
    public <T> T get(
            Class<T> elementClass,
            String url,
            Map<String, String> headers
    ) throws HttpClient4xxErrorException {
        HttpHeaders httpHeaders = new HttpHeaders();
        headers.forEach(httpHeaders::add);
        var request = new HttpEntity<>("", httpHeaders);
        try {
            return restTemplate
                    .exchange(url, HttpMethod.GET, request, elementClass)
                    .getBody()
            ;
        } catch (HttpClientErrorException clientErrorException) {
            if (! clientErrorException.getStatusCode().is4xxClientError()) {
                throw clientErrorException;
            }
            throw new HttpClient4xxErrorException(
                    clientErrorException.getMessage(),
                    clientErrorException.getStatusCode().value(),
                    clientErrorException
            );
        }
    }

    protected RestTemplate getWebClient() throws NoSuchAlgorithmException, KeyManagementException {
        SSLContext sc = SSLContext.getInstance("TLSv1.2");
        sc.init(null, TRUST_MANAGERS, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        var template = new RestTemplate();
        template.getMessageConverters().add(new org.springframework.http.converter.FormHttpMessageConverter());

        return template;
    }
}
