package com.zfpoly.siret.server.adapter.company.service.fetcher.insee.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.HashMap;

@Data
@Accessors(chain = true)
public class InseeEtablissement {
    private String siret;
    private String nic;
    private String dateCreationEtablissement;
    private InseeUniteLegale uniteLegale;
    private HashMap<String, String> adresseEtablissement;

    public InseeEtablissement() {
        adresseEtablissement = new HashMap<>();
    }
}
