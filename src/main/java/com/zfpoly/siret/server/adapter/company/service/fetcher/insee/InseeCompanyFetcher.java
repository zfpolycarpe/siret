package com.zfpoly.siret.server.adapter.company.service.fetcher.insee;

import com.zfpoly.siret.domain.company.model.Company;
import com.zfpoly.siret.domain.company.ports.server.service.fetcher.CompanyFetcherService;
import com.zfpoly.siret.domain.shared.ports.client.response.ResponseStatus;
import com.zfpoly.siret.domain.shared.ports.client.response.exception.ResponseException;
import com.zfpoly.siret.domain.shared.ports.server.http_client.HttpClient;
import com.zfpoly.siret.domain.shared.ports.server.http_client.exception.HttpClient4xxErrorException;
import com.zfpoly.siret.domain.shared.ports.server.http_client.exception.HttpClient5xxErrorException;
import com.zfpoly.siret.server.adapter.company.service.fetcher.insee.config.InseeConfig;
import com.zfpoly.siret.server.adapter.company.service.fetcher.insee.response.Insee4xxError;
import com.zfpoly.siret.server.adapter.company.service.fetcher.insee.response.InseeResponse;
import com.zfpoly.siret.server.adapter.company.service.fetcher.insee.response.InseeTokenResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class InseeCompanyFetcher implements CompanyFetcherService {

    private final HttpClient httpClient;
    private final InseeConfig inseeConfig;

    @Autowired
    public InseeCompanyFetcher(
            @Qualifier("webClientHttpClient") HttpClient httpClient,
            InseeConfig inseeConfig
    ) {
        this.httpClient = httpClient;
        this.inseeConfig = inseeConfig;
    }

    /**
     * {@inheritDoc}
     */
    public Optional<Company> fetch(String siret) throws ResponseException {
        try {
            return httpClient.get(
                    InseeResponse.class,
                    inseeConfig.siretUrl(siret),
                    inseeConfig.siretHeader(getToken())
            ).toCompany();
        } catch (HttpClient4xxErrorException httpClient4xxErrorException) {
            switch (httpClient4xxErrorException.getStatus()) {
                case 404 -> {
                    return Optional.empty();
                }
                case 400 -> throw new ResponseException(
                        "Invalid siret request with value: [" + siret + "]",
                        ResponseStatus.BAD_REQUEST,
                        httpClient4xxErrorException.getBody(),
                        Insee4xxError.class
                );
                default -> throw httpClient4xxErrorException;
            }
        }
    }

    protected String getToken() {
        var inseeToken = this.httpClient.post(
                InseeTokenResponse.class,
                inseeConfig.tokenUrl(),
                inseeConfig.tokenBody(),
                inseeConfig.tokenHeader()
        );

        if (Objects.isNull(inseeToken.getAccess_token())) {
            throw new HttpClient5xxErrorException("Invalid insee token !", 500);
        }

        return inseeToken.getAccess_token();
    }
}




















