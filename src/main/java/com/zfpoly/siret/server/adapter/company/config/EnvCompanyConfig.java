package com.zfpoly.siret.server.adapter.company.config;

import com.zfpoly.siret.domain.company.ports.server.config.CompanyConfig;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@Getter
@Component
public class EnvCompanyConfig implements CompanyConfig {

    @Value("${siret.company.csv.name:company.csv}")
    private String csvName;

    /**
     * {@inheritDoc}
     */
    public Path csvPath() throws IOException {
        var path = Paths.get(getCsvName());
        if (Objects.nonNull(path.getParent()) && Files.notExists(path.getParent())) {
            Files.createDirectories(path.getParent());
        }
        if (Files.notExists(path)) {
            Files.createFile(path);
        }

        return path;
    }
}
