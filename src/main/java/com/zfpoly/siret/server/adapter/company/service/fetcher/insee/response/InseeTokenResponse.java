package com.zfpoly.siret.server.adapter.company.service.fetcher.insee.response;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class InseeTokenResponse {
    private String access_token;
}
