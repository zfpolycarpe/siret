package com.zfpoly.siret.server.adapter.company.service.fetcher.insee.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class InseeUniteLegale {
    private String denominationUniteLegale;
}
