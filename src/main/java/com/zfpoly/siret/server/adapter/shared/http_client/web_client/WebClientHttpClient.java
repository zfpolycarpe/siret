package com.zfpoly.siret.server.adapter.shared.http_client.web_client;

import com.zfpoly.siret.domain.shared.ports.server.http_client.HttpClient;
import com.zfpoly.siret.domain.shared.ports.server.http_client.exception.HttpClient4xxErrorException;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.net.ssl.SSLException;
import java.util.Map;
import java.util.Optional;

@Service
public class WebClientHttpClient implements HttpClient {

    private final WebClient.Builder webClient;

    public WebClientHttpClient() throws SSLException {
        this.webClient = getWebClient();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T post(
            Class<T> elementClass,
            String url,
            Map<String, Object> body,
            Map<String, String> headers
    ) {
        LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        body.forEach(map::add);
        return webClient
                .baseUrl(url)
                .build()
                .post()
                .headers(httpHeaders -> headers.forEach(httpHeaders::add))
                .body(BodyInserters.fromValue(map))
                .retrieve()
                .onStatus(HttpStatusCode::is4xxClientError, clientResponse -> clientResponse
                        .bodyToMono(String.class)
                        .flatMap(error -> build4xxError(clientResponse, error)))
                .bodyToMono(elementClass)
                .block();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T get(
            Class<T> elementClass,
            String url,
            Map<String, String> headers
    ) {
        return webClient
                .baseUrl(url)
                .build()
                .get()
                .headers(httpHeaders -> headers.forEach(httpHeaders::add))
                .retrieve()
                .onStatus(HttpStatusCode::is4xxClientError, clientResponse -> clientResponse
                        .bodyToMono(String.class)
                        .flatMap(error -> build4xxError(clientResponse,  error))
                )
                .bodyToMono(elementClass)
                .block()
                ;
    }

    protected WebClient.Builder getWebClient() throws SSLException {
        var sslContext = SslContextBuilder
                .forClient()
                .trustManager(InsecureTrustManagerFactory.INSTANCE)
                .build();
        var httpClient = reactor.netty.http.client.HttpClient
                .create()
                .secure(sslContextSpec -> sslContextSpec.sslContext(sslContext));

        return WebClient.builder().clientConnector(new ReactorClientHttpConnector(httpClient));
    }

    protected <R extends Throwable> Mono<R> build4xxError(ClientResponse clientResponse, String error) {
        int statusCode = clientResponse.statusCode().value();
        var errorMessage = Optional
                .ofNullable(HttpStatus.resolve(statusCode))
                .map(HttpStatus::getReasonPhrase)
                .orElseGet(() -> "Error " + statusCode + " occured")
                ;

        return Mono.error(new HttpClient4xxErrorException(errorMessage, statusCode, error));
    }
}
