package com.zfpoly.siret.server.adapter.company.repository.csv;

import lombok.Getter;

import java.io.IOException;

@Getter
public class CsvCompanyRepositoryException extends RuntimeException {

    public CsvCompanyRepositoryException(IOException ioException) {
        super(CsvCompanyRepositoryException.class + ":" + ioException.getMessage(), ioException);
    }
}
