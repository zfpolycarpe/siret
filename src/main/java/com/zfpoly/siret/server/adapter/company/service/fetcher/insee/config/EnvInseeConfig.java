package com.zfpoly.siret.server.adapter.company.service.fetcher.insee.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Getter
@Service
public class EnvInseeConfig implements InseeConfig {
    @Value("${siret.insee.customer.key:}")
    private String key;

    @Value("${siret.insee.customer.secret:}")
    private String secret;

    @Value("${siret.insee.url.token:https://api.insee.fr/token}")
    private String urlToken;

    @Value("${siret.insee.url.siret:https://api.insee.fr/entreprises/sirene/V3/siret/}")
    private String urlSiret;

    public String tokenUrl() {
        return getUrlToken();
    }

    public String siretUrl(String siret) {
        return getUrlSiret() + siret;
    }

    public Map<String, Object> tokenBody() {
        Map<String, Object> body = new HashMap<>();
        body.put("grant_type", "client_credentials");

        return body;
    }

    public Map<String, String> tokenHeader() {
        Map<String, String> headers = new HashMap<>();
        headers.put(HttpHeaders.AUTHORIZATION, "Basic " + Base64
                .getEncoder()
                .encodeToString((getKey() + ":" + getSecret()).getBytes()));
        headers.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE);

        return headers;
    }

    public Map<String, String> siretHeader(String accessToken) {
        Map<String, String> headers = new HashMap<>();
        headers.put(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        headers.put(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);

        return headers;
    }
}
