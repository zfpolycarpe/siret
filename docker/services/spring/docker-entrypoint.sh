#!/bin/sh
set -eux;

echo $INSEE_CUSTOMER_KEY
echo $INSEE_CUSTOMER_SECRET
if [ ! -f './src/main/resources/application.properties' ]; then
      cp ./src/main/resources/application.properties.example ./src/main/resources/application.properties
      sed -i "s/INSEE_CUSTOMER_KEY/$INSEE_CUSTOMER_KEY/" ./src/main/resources/application.properties
      sed -i "s/INSEE_CUSTOMER_SECRET/$INSEE_CUSTOMER_SECRET/" ./src/main/resources/application.properties
fi

mvn spring-boot:run
