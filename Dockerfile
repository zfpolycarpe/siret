FROM openjdk:18-slim-bullseye as builder

ARG APP_USER=1000
ARG APP_GROUP=1000

ENV INSEE_CUSTOMER_KEY=m47ioOiKWr5izPfycm_QeQrtXUga
ENV INSEE_CUSTOMER_SECRET=yv_Dxq65mhWSlS6jwz3rW_onBaga

RUN apt-get update && apt-get install -y maven && rm -rf /var/lib/apt/lists/*

COPY ./app-package.sh /usr/local/bin/app-package.sh
RUN chmod +x /usr/local/bin/app-package.sh

RUN groupadd -g $APP_GROUP -r user && useradd --no-log-init -m -s /bin/bash -u $APP_USER -r -g user user

COPY ./ /home/user/app
RUN chown -R user:user /home/user/app
USER user
RUN app-package.sh

FROM openjdk:18-slim-bullseye

ARG APP_USER=1000
ARG APP_GROUP=1000

RUN groupadd -g $APP_GROUP -r user && useradd --no-log-init -m -s /bin/bash -u $APP_USER -r -g user user
USER user

COPY --from=builder /home/user/app/target/siret-1.0.0.jar /home/user/siret-1.0.0.jar

WORKDIR /home/user
VOLUME /home/user/.data-app

EXPOSE 8080

CMD ["java", "-Dserver.port=8080", "-jar", "siret-1.0.0.jar"]